import { Component, OnInit } from '@angular/core';
import {ImagesService} from '../images.service'

@Component({
  selector: 'app-images',
  templateUrl: './images.component.html',
  styleUrls: ['./images.component.css']
})
export class ImagesComponent implements OnInit {

  images:any
  constructor(private imagesService:ImagesService) {
    this.getImages()
  }

  ngOnInit(): void {
  }

  getImages(){
    this.imagesService.getImages().subscribe((response)=>{
      this.images = response['data']
      console.log(this.images)
    }, error => {
      console.log(error)
    })
  }

}
