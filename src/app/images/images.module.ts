import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { ImagesComponent } from './images/images.component';
import { ImagesDetailComponent } from './images-detail/images-detail.component';

const routes: Routes = [{path:'images',component:ImagesComponent}];

@NgModule({
  declarations: [ImagesComponent, ImagesDetailComponent],
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forChild(routes)
    
  ]
})
export class ImagesModule { }
