import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-images-detail',
  templateUrl: './images-detail.component.html',
  styleUrls: ['./images-detail.component.css']
})
export class ImagesDetailComponent implements OnInit {

  id:string
  url:string
  @Input() dataImage: any;
  currentImage:any
  constructor() { 
    
  }

  ngOnInit(): void {
    this.currentImage = this.dataImage
    this.getImageInformation(this.currentImage)
  }

  getImageInformation(data){
    console.log('paso por aca')
    this.url = data['url']
    this.id = data['id']
  }

}
