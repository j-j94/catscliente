import { Component, OnInit } from '@angular/core';
import { CatsServiceService} from '../cats-service.service'
@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  name:string
  type:string
  age:string
  photo:string
  cats:Array<any>
  constructor(private service:CatsServiceService) { 
    this.cats=[]
    this.name=""
    this.type=""
    this.age=""
    this.photo=""
  }

  ngOnInit(): void {
    this.getCats()
  }

  getCats(){
    this.service.getCats().subscribe((response)=>{
      console.log(response['data'])
      this.cats = response['data']
    },error =>{
      console.log(error)
    }
    )
  }

  delete(data){
    console.log(data)
    this.service.deleteCat(data['_id']).subscribe((response)=>{
      console.log(response)
      this.cats = this.filterArray(this.cats,data)
    },error => {
      console.log(error)
    })
   

  }

  filterArray(array,cat){
    return array.filter((catEntry)=>{
      console.log(catEntry)
      return catEntry['_id'] != cat['_id']
    })
  }

  createCat() {
    alert('create cat')
    let data = {
      name: this.name,
      type: this.type,
      age: this.age,
      photo: ""
    }

    this.service.createCat(data).subscribe((response)=>{
      console.log(response)
      this.cats.push(response['data'])
    },error =>{
      console.log(error)
    })

  }

  changeName(event) {
    this.name = event.target.value
  }

  changeType(event) {
    this.type = event.target.value
  }
  changeAge(event) {
    this.age = event.target.value
  }

}
