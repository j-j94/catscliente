import { Component, Input, OnInit } from '@angular/core';
import {CatsServiceService} from '../cats-service.service';

@Component({
  selector: 'app-data',
  templateUrl: './data.component.html',
  styleUrls: ['./data.component.css']
})
export class DataComponent implements OnInit {

  @Input() dataCat: any;
  currentCat:any 
  constructor(private service:CatsServiceService) { }

  ngOnInit(): void {
    this.currentCat = this.dataCat  
    console.log(this.currentCat)
  }

  updateCat(){
    alert('se actualizará el registro del gato')
    console.log(this.currentCat)  
    this.service.updateCat(this.currentCat).subscribe((response)=>{
      console.log(response)
    },error =>{
      console.log(error)
    }
    
    )
  }

  changeName(event){
    console.log(event.target.value)
    this.currentCat['name'] = event.target.value
    console.log(this.currentCat)
  }

  changeType(event){
    this.currentCat['type'] = event.target.value
  }

  changeAge(event){
    this.currentCat['age'] = event.target.value
  }

}
