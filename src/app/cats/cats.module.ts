import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './main/main.component';
import { DataComponent } from './data/data.component';

const routes: Routes = [{path:'gatos',component:MainComponent}];

@NgModule({
  declarations: [MainComponent, DataComponent],
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(routes)
  ]
})
export class CatsModule { }
