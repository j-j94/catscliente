import { Injectable } from '@angular/core';
import { HttpClient,HttpParams } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class CatsServiceService {

  url = 'http://localhost:3000/'
  constructor(private httpClient:HttpClient) { }

  getCats(){
    return this.httpClient.get(`${this.url}gatos`)
  }

  createCat(catData){
    return this.httpClient.post(`${this.url}gatos/`,catData)
  }

  updateCat(cat){
    return this.httpClient.put(`${this.url}gatos/${cat['_id']}`,cat)
  }

  deleteCat(id){
    return this.httpClient.delete(`${this.url}gatos/${id}`)
  }

}
